package com.example.moshi.Network

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitInstance {
    const val BASE_URL="https://run.mocky.io/"
    val newsApi: newsApi  by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(newsApi::class.java)
    }
}