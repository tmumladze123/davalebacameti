package com.example.moshi.Network

import com.example.moshi.Data.News
import retrofit2.Response
import retrofit2.http.GET


interface newsApi {
    @GET(API_URL)
    suspend fun getNews() : Response<News>

    companion object {
        const val API_URL="/v3/c111ca66-46e7-400e-ab5d-809865408c66"
    }

}