package com.example.moshi.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.moshi.Data.News
import com.example.moshi.Extensions.setPhoto
import com.example.moshi.databinding.NewsItemBinding

class NewsAdapter(val data: News) :  RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: NewsItemBinding) :RecyclerView.ViewHolder(binding.root){

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsAdapter.ViewHolder
    =ViewHolder(NewsItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun onBindViewHolder(holder: NewsAdapter.ViewHolder, position: Int) {
        holder.binding.ivNews.setPhoto(data[position].imgUrl)
        holder.binding.tvDesc.text=data[position].description
        holder.binding.tvTitle.text=data[position].title
    }

    override fun getItemCount(): Int =data.size
}