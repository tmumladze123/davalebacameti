package com.example.moshi.Fragments

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moshi.Adapters.NewsAdapter
import com.example.moshi.Data.News
import com.example.moshi.Models.NewsViewModel
import com.example.moshi.Network.manageResponse
import com.example.moshi.databinding.FragmentNewsBinding

class NewsFragment : BaseFragment<FragmentNewsBinding>(FragmentNewsBinding::inflate) {
    private val newsModel: NewsViewModel by viewModels()
    override fun start() {
        newsModel.connect()
        getResult()

    }
    fun initAdapter(data:News)
    {
        var adapter=NewsAdapter(data)
        binding.rvNews.adapter=adapter
        binding.rvNews.layoutManager= LinearLayoutManager(binding.root.context)
    }
    fun getResult()
    {
        newsModel.newsResource.observe(viewLifecycleOwner,{
            when(it)
            {
                is manageResponse.Success -> { success(it.data!!) }
                is manageResponse.Error -> { error(it.message) }
                is manageResponse.Loading -> { loading() }
            }
        })
    }
    fun success(data:News)
    {
        initAdapter(data)
    }
    fun error(message:String?)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
    fun loading()
    {
    }
}