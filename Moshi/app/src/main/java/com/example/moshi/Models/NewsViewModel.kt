package com.example.moshi.Models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moshi.Data.News
import com.example.moshi.Network.RetrofitInstance
import com.example.moshi.Network.manageResponse
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class NewsViewModel : ViewModel()  {
    private  var _newsResource = MutableLiveData<manageResponse<News>>()
    val newsResource: LiveData<manageResponse<News>>
        get() = _newsResource

     fun connect()
    { viewModelScope.launch { withContext(IO){ getNews() } } }
    private suspend fun getNews()
    {
        try {
            _newsResource.postValue(manageResponse.Loading<News>())
            var result=RetrofitInstance.newsApi.getNews()
            var body=result.body()
            if(result.isSuccessful && body!=null)
                _newsResource.postValue(manageResponse.Success<News>(body))
            else
                _newsResource.postValue(manageResponse.Error<News>("ERROR",body))
            _newsResource.postValue(manageResponse.Loading<News>())
        }
        catch (e:Exception) { println(e) }
    }


}