package com.example.moshi.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.example.moshi.R

typealias Inflater <RD> = (LayoutInflater, ViewGroup?, Boolean) -> RD
typealias model <VM> = () -> VM
abstract class BaseFragment <VB : ViewBinding> (var inflate: Inflater<VB>) : Fragment() {

    private var _binding: VB? = null
    open val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding=inflate.invoke(inflater,container,false)
        start()
        return inflater.inflate(R.layout.fragment_base, container, false)
    }
    abstract fun start()
    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }

}