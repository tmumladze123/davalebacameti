package com.example.moshi.Network


sealed class manageResponse<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : manageResponse<T>(data)

    class Error<T>(message: String?, data: T? = null) : manageResponse<T>(data, message)

    class Loading<T> (): manageResponse<T>()

}